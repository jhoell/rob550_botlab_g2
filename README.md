BotLab - Section 1 Group 2: jbamin, jhoell, mangelso

======= Group 2 Botlab Readme ======= 
Our Botlab code is divided into three executables and one library. All files are written in C and compiled with gcc and make.

The c-files corresponding to executables are as follows:
- botlab.c
- camera_lidar.c
- odometry.c

xty.c and xty.h are the files for 3DOF versions of the ssc algorithms for calculating pose and covariance.

======= Build Instructions ======= 
To build the code, run <make> from the top level. Since part of the code runs on the laptop and part runs on the maebot, the code needs to be compiled on both platforms. 
Make sure to not compile the maebot code from within the (via sshfs) mounted directory on the laptop because that will compile for a x86 processor and not ARM.


======= Run Instructions ======= 
Both a laptop and a maebot are needed to correctly run all of the code for this project. On both systems, first run the setEnv.sh script. Then navigate to the bin folder on each system. On the maebot, execute the following commands in order (can us & so that they run in the background):

	- bot-lcm-tunnel (&)
		This is the start of a direct LCM connection between the laptop and the maebot.
	- bot-procman-deputy (&) 
		This is the program in charge of managing processes on the maebot

On the laptop, run the following commands:

	- bot-lcm-tunnel <IP adress of bot> (&)
		This is the second half of the direct LCM connection between the two systems.
	- bot-procman-deputy (&)
		This manages the processes running on the laptop
	- bot-procman-sheriff <config-file> 
		This program controls both deputies and is the main user interface for controlling the various processes. The config file is located in the /config folder.

Running bot-procman-sheriff will cause a GUI to open up. From within this gui, the various programs associated with botlab can be initiated.

First, run the drivers tab by right clicking it and  aselecting "start" from the menu that pops up. 

	=== botlab_odometry ===
Next, start the botlab_odometry process. This program listens to the maebot motor and sensor feedback LCM channels. It uses this information to calculate the current xyt pose and covariance matrix of the robot. One can configure the odometry via a number of different flags, which are listed below:

	1) --alpha=[value]  --> sets the alpha term of the encoder tick noise due to wheel slip. This influences the covariance projection. 

	2) --beta=[value] --> sets the beta term of the encoder tick noise due to side slip. This also influences the covariance projection.

	3) --use-gyro --> makes odometry use the bias-compensated gyro angle for the robot's heading which is much more accurate and estimating the heading using the encoder values

	4) -w / --write-file  --> flag that turns on recording of gyro-specific values to a csv file which can later be processed in matlab. The default file name is "gyro_data.csv" but can be changed via the --filename flag

	5) --filename=[name] --> sets the name of the file to which the gyro information will be written. This allows for multiple data sets to be recorded very quickly.

	=== botlab_app ===
The botlab_app program runs on the maebot and controls the visualization of the robot as well as the autonomous control (not implemented for this botlab). It receives xyt_pose information from botlab_odometry and uses that to render the path the robot has taken as well as draw covariance ellipses (shows uncertainty of robot position) every 10 cm of motion. It also projects the lidar points into the global camera frame.


	=== botlab_camera_lidar ===
This program is separate from botlab_app but also runs on the maebot. The two can be run at the same time but that is not necessary and not recommended. This program projects the lidar points into the camera image. The two most important flags for this program are as follows:

	1) --url=[url] --> specify the camera url. On the maebots, this usually is "pgusb://"

	2) --config=[location] --> location of the camera intrinsic and distortion correction parameters. Neceesary to correct the image for display.


	=== vx_remote_viewer ===
This program runs on the laptop. It is connected to either botlab_app or botlab_camera_lidar through the vx backend (uses UDP) and is in charge of rendering the visualizations on the host computer. This way the maebot does not have to spend limited processing time and power on rendering the visualization.
