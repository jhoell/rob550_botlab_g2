#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <math.h>

#include "common/getopt.h"
#include "math/gsl_util_matrix.h"
#include "math/gsl_util_blas.h"
#include "math/gsl_util_vector.h"
#include "math/math_util.h"

#include "lcmtypes/maebot_motor_feedback_t.h"
#include "lcmtypes/maebot_sensor_data_t.h"
#include "lcmtypes/pose_xyt_t.h"

#include "xyt.h"

#define ALPHA_STRING          "1.0"  // longitudinal covariance scaling factor
#define BETA_STRING           "1.0"  // lateral side-slip covariance scaling factor
#define GYRO_RMS_STRING       "1.0"    // [deg/s]
#define FILENAME_STR		  "gyro_data.csv"

#define BASELINE 0.080445f

typedef struct state state_t;
struct state {
    getopt_t *gopt;

    lcm_t *lcm;
    const char *odometry_channel;
    const char *feedback_channel;
    const char *sensor_channel;

    // odometry params
    double meters_per_tick; // conversion factor that translates encoder pulses into linear wheel displacement
    double alpha;
    double beta;
    double gyro_rms;

    bool use_gyro;
    int64_t dtheta_utime;
    double dtheta;
    double dtheta_sigma;

    bool gyro_initialized;
    bool gyro_prev_initialized;
    int num_calib_points;
    double *gyro_calib_data;
    double *gyro_calib_data_time;
    int64_t init_utime;
    double prev_gyro;
    double calc_slope;


    bool record_gyro;
    FILE * gyro_file;


    // need previous left and right encoder tick values to find difference
    int64_t prev_tick_L;
    int64_t prev_tick_R;
    bool ticks_initialized;

    double xyt[3]; // 3-dof pose
    double Sigma[3*3];
};

static void
motor_feedback_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                        const maebot_motor_feedback_t *msg, void *user)
{
    state_t *state = user;

    if(state->ticks_initialized==false)
    {
        state->prev_tick_L = msg->encoder_left_ticks;
        state->prev_tick_R = msg->encoder_right_ticks;
        state->ticks_initialized = true;
        return;
    }

    // find encoder tick differences
    int64_t d_tick_L = msg->encoder_left_ticks - state->prev_tick_L;
    int64_t d_tick_R = msg->encoder_right_ticks - state->prev_tick_R;
    // update previous tick values
    state->prev_tick_R = msg->encoder_right_ticks;
    state->prev_tick_L = msg->encoder_left_ticks;

    //compute actual distances
    double d_L = (double)d_tick_L*state->meters_per_tick;
    double d_R = (double)d_tick_R*state->meters_per_tick;

    // place into vector for matrix multiplication
    gsl_vector * ticks = gsl_vector_alloc(3);
    gsl_vector_set(ticks,0,d_L);
    gsl_vector_set(ticks,1,d_R);
    gsl_vector_set(ticks,2,0); //dS mean is always 0

    // create wheel distance difference to x,y,t diff transformation matrix
    gsl_matrix * A = gsl_matrix_calloc(3,3);
    gsl_matrix_set(A,0,0,0.5);
    gsl_matrix_set(A,0,1,0.5);
    gsl_matrix_set(A,1,2,1); //since dy = ds
    gsl_matrix_set(A,2,0,-1.0/BASELINE);
    gsl_matrix_set(A,2,1,1.0/BASELINE);


    // create 3x3 covariance matrix for current motion -- assume independence of dL, dR, and dS
    double var_dL = pow(state->alpha*fabs(d_L),2);
    double var_dR = pow(state->alpha*fabs(d_R),2);
    double var_dS = pow(state->beta*fabs(d_L+d_R),2);
    gsl_matrix * covar_incr_wh_ticks = gsl_matrix_calloc(3,3);
    gsl_matrix_set(covar_incr_wh_ticks,0,0,var_dL);
    gsl_matrix_set(covar_incr_wh_ticks,1,1,var_dR);
    gsl_matrix_set(covar_incr_wh_ticks,2,2,var_dS);

    // propogate covariance from wheels to incremental position
    // since its a linear relationship, sigma_incr_pos = A*sigma_wheel*A^T
    gsl_matrix * covar_incr_xyt = gsl_matrix_alloc(3,3);
    gslu_blas_mmmT (covar_incr_xyt,A,covar_incr_wh_ticks,A,NULL);

    // calculate incremental change in xyt
    gsl_vector * xyt_diff = gsl_vector_alloc(3);
    gslu_blas_mv(xyt_diff,A,ticks);


    if(state->use_gyro && state->gyro_initialized) {
        gsl_vector_set(xyt_diff,2,state->dtheta);
        gsl_matrix_set(covar_incr_xyt,2,2,state->dtheta_sigma);
    }

    // find new xyt based on previous position and incremental change
    gsl_vector * xyt_result = gsl_vector_alloc(3);
    gsl_vector * cur_pos = gsl_vector_alloc(3);
    memcpy(cur_pos->data,state->xyt,sizeof state->xyt);
    gsl_matrix * J_plus = gsl_matrix_calloc(3,6);
    xyt_head2tail_gsl(xyt_result,J_plus,cur_pos,xyt_diff);

    // find new covar matrix using previous covar matrix and covar of incremental movement

    // first need to create 6 x 6 super covariance matrix 
    gsl_matrix * super_Sigma = gsl_matrix_calloc(6,6);
    // top left 3x3 portion of super covar is old covariance matrix
    int i = 0,j=0;
    for(i=0;i<3;i++)
        for(j=0;j<3;j++)
            gsl_matrix_set(super_Sigma,i,j,state->Sigma[i*3+j]);
    // bottom right 3x3 portion of super covar is incremental covariance matrix
    for(i=0;i<3;i++)
        for(j=0;j<3;j++)
            gsl_matrix_set(super_Sigma,3+i,3+j,gsl_matrix_get(covar_incr_xyt,i,j));

    // now do covar propagation
    gsl_matrix * new_Sigma = gsl_matrix_alloc(3,3);
    gslu_blas_mmmT (new_Sigma,J_plus,super_Sigma,J_plus,NULL);


    // copy values over to state
    memcpy(state->Sigma,new_Sigma->data,sizeof state->Sigma);
    memcpy(state->xyt,xyt_result->data,sizeof state->xyt);

    gslu_vector_printf(xyt_result,"pose [x, y, theta]");

    gsl_vector_free(xyt_result);
    gsl_vector_free(cur_pos);
    gsl_vector_free(ticks);
    gsl_matrix_free(A);
    gsl_matrix_free(J_plus);
    gsl_matrix_free(covar_incr_wh_ticks);
    gsl_matrix_free(covar_incr_xyt);
    gsl_matrix_free(super_Sigma);
    gsl_matrix_free(new_Sigma);


    // publish
    pose_xyt_t odo = { .utime = msg->utime };
    memcpy (odo.xyt, state->xyt, sizeof state->xyt);
    memcpy (odo.Sigma, state->Sigma, sizeof state->Sigma);
    pose_xyt_t_publish (state->lcm, state->odometry_channel, &odo);
}

static void
sensor_data_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                     const maebot_sensor_data_t *msg, void *user)
{
    state_t *state = user;

    if (!state->use_gyro)
        return;


    // if this is the very first time we are doing this, record the value and then return
    if (!state->gyro_prev_initialized) {
        state->prev_gyro = (double)msg->gyro_int[2]/(1.0e6*131);
        state->gyro_prev_initialized = true;
        state->init_utime = msg->utime;
        state->dtheta_utime = msg->utime;
        return;
    }
    
    if (!state->gyro_initialized) {
        static int gyro_index = 0;
        state->gyro_calib_data[gyro_index] = (double)msg->gyro_int[2]/(1.0e6*131);
        state->gyro_calib_data_time[gyro_index] = (double)(msg->utime-state->init_utime)/1.0e6; // time in seconds
        gyro_index++;

        if(gyro_index==state->num_calib_points){
            // find best fit line.....
            gsl_matrix * A = gsl_matrix_alloc(state->num_calib_points,2);
            gsl_vector *x = gsl_vector_alloc(2); // [m b]^T
            gsl_vector *b = gsl_vector_calloc(state->num_calib_points);
            memcpy(b->data,state->gyro_calib_data,state->num_calib_points*sizeof(double));
            int i;
            for (i=0;i<state->num_calib_points;i++) {
            	gsl_matrix_set(A,i,0,state->gyro_calib_data_time[i]);
            	gsl_matrix_set(A,i,1,1);
            }
			// x = (A^T*A)^-1*A^T*b
			gslu_blas_mv(x, gslu_blas_mmT_alloc(gslu_matrix_inv_alloc(gslu_blas_mTm_alloc(A,A)),A),b);

			state->calc_slope = gsl_vector_get(x,0); // really just need m part. the y-intercept is in this case not important

		    gsl_vector_free(x);
		    gsl_vector_free(b);
		    gsl_matrix_free(A);

        	state->gyro_initialized = true;
        }  
		state->prev_gyro = (double)msg->gyro_int[2]/(1.0e6*131);
        state->dtheta_utime = msg->utime;

        //don't have unbiased data yet, so just print 0s there
        // order is : utime, biased, unbiased
		if(state->record_gyro)
        	fprintf(state->gyro_file,"%f,%f,%f,%f\n",(double)state->dtheta_utime/1.0e6,state->prev_gyro,0.0,state->xyt[2]);
    }
    else { //gyro has been initialized!
    	//need to convert angle to radians!!!!
    	double bias_offset = state->calc_slope*((double)msg->utime - (double)state->dtheta_utime)/1.0e6;
    	double angle_difference = (double)(msg->gyro_int[2])/(1.0e6*131) - state->prev_gyro;
		state->dtheta = (M_PI/180.0)*(angle_difference-bias_offset);



		state->prev_gyro = (double)msg->gyro_int[2]/(1.0e6*131);
		state->dtheta_utime = msg->utime;

        //don't have unbiased data yet, so just print 0s there
        // order is : utime, biased, unbiased
		if(state->record_gyro)
        	fprintf(state->gyro_file,"%f,%f,%f,%f\n",(double)state->dtheta_utime/1.0e6,state->prev_gyro,state->dtheta,state->xyt[2]);

		printf("dtheta: %f\n", state->dtheta);
    }
}

int
main (int argc, char *argv[])
{
    // so that redirected stdout won't be insanely buffered.
    setvbuf (stdout, (char *) NULL, _IONBF, 0);

    state_t *state = calloc (1, sizeof *state);

    state->meters_per_tick = 2.234e-4; //1.0; // IMPLEMENT ME
    printf("meters per tick %f\n",state->meters_per_tick);

    state->gopt = getopt_create ();
    getopt_add_bool   (state->gopt, 'h', "help", 0, "Show help");
    getopt_add_bool   (state->gopt, 'g', "use-gyro", 0, "Use gyro for heading instead of wheel encoders");
    getopt_add_string (state->gopt, '\0', "odometry-channel", "BOTLAB_ODOMETRY", "LCM channel name");
    getopt_add_string (state->gopt, '\0', "feedback-channel", "MAEBOT_MOTOR_FEEDBACK", "LCM channel name");
    getopt_add_string (state->gopt, '\0', "sensor-channel", "MAEBOT_SENSOR_DATA", "LCM channel name");
    getopt_add_double (state->gopt, '\0', "alpha", ALPHA_STRING, "Longitudinal covariance scaling factor");
    getopt_add_double (state->gopt, '\0', "beta", BETA_STRING, "Lateral side-slip covariance scaling factor");
    getopt_add_double (state->gopt, '\0', "gyro-rms", GYRO_RMS_STRING, "Gyro RMS deg/s");
    getopt_add_bool	  (state->gopt, 'w',  "write-file", 0, "Write gyro data to a csv file for easier data processing");
    getopt_add_string (state->gopt, '\0', "filename", FILENAME_STR, "Filename of gyro data csv file");

    if (!getopt_parse (state->gopt, argc, argv, 1) || getopt_get_bool (state->gopt, "help")) {
        printf ("Usage: %s [--url=CAMERAURL] [other options]\n\n", argv[0]);
        getopt_do_usage (state->gopt);
        exit (EXIT_FAILURE);
    }

    state->use_gyro = getopt_get_bool (state->gopt, "use-gyro");
    state->odometry_channel = getopt_get_string (state->gopt, "odometry-channel");
    state->feedback_channel = getopt_get_string (state->gopt, "feedback-channel");
    state->sensor_channel = getopt_get_string (state->gopt, "sensor-channel");
    state->alpha = getopt_get_double (state->gopt, "alpha");
    state->beta = getopt_get_double (state->gopt, "beta");
    state->gyro_rms = getopt_get_double (state->gopt, "gyro-rms") * DTOR;

    state->record_gyro = getopt_get_bool(state->gopt, "write-file");
    state->gyro_file = fopen(getopt_get_string(state->gopt,"filename"),"w");
    printf("gyro output file: %p\n", state->gyro_file);

    state->ticks_initialized = false;
    state->prev_tick_R = 0;
    state->prev_tick_L = 0;

    state->gyro_initialized = false;
    state->gyro_prev_initialized = false;
    state->num_calib_points = 30*20; // 30 seconds of data roughly
    state->gyro_calib_data = malloc(state->num_calib_points*sizeof(double));
    state->gyro_calib_data_time = malloc(state->num_calib_points*sizeof(double));
    state->calc_slope = 0;

    // initialize LCM
    state->lcm = lcm_create (NULL);
    maebot_motor_feedback_t_subscribe (state->lcm, state->feedback_channel,
                                       motor_feedback_handler, state);
    maebot_sensor_data_t_subscribe (state->lcm, state->sensor_channel,
                                    sensor_data_handler, state);

    printf ("ticks per meter: %f\n", 1.0/state->meters_per_tick);

    while (1)
        lcm_handle (state->lcm);
}
