#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "math/gsl_util_vector.h"
#include "math/gsl_util_matrix.h"
#include "math/gsl_util_blas.h"

#include "xyt.h"

int
xyt_rbt (double T[3*3], const double X_ij[3])
{
  double x_ij = X_ij[0];
  double y_ij = X_ij[1];
  double th_ij = X_ij[2];
  double c_th = cos(th_ij);
  double s_th = sin(th_ij);

  T[0 + 0*3] = c_th;
  T[1 + 0*3] = -s_th;
  T[2 + 0*3] = x_ij;
  T[0 + 1*3] = s_th;
  T[1 + 1*3] = c_th;
  T[2 + 1*3] = y_ij;
  T[0 + 2*3] = 0;
  T[1 + 2*3] = 0;
  T[2 + 2*3] = 1;

  return GSL_SUCCESS;
}

int
xyt_rbt_gsl (gsl_matrix *T, const gsl_vector *X_ij)
{
    assert (T->size1 == 3 && T->size2 == 3);
    assert (X_ij->size == 3 && X_ij->stride == 1);

    return xyt_rbt (T->data, X_ij->data);
}


int
xyt_inverse (double X_ji[3], double J_minus[3*3], const double X_ij[3])
{
  double x_ij = X_ij[0];
  double y_ij = X_ij[1];
  double phi_ij = X_ij[2];
  double c_phi_ij = cos(phi_ij);
  double s_phi_ij = sin(phi_ij);

  X_ji[0] = -x_ij*c_phi_ij - y_ij*s_phi_ij;
  X_ji[1] = x_ij*s_phi_ij - y_ij*c_phi_ij;
  X_ji[2] = -phi_ij;

  if (J_minus != NULL) {
    J_minus[0] = -c_phi_ij;
    J_minus[1] = -s_phi_ij;
    J_minus[2] = X_ji[1];

    J_minus[3] = s_phi_ij;
    J_minus[4] = -c_phi_ij;
    J_minus[5] = -X_ji[0];
    
    J_minus[6] = 0;
    J_minus[7] = 0;
    J_minus[8] = -1;
  }
  return GSL_SUCCESS;
}

int
xyt_inverse_gsl (gsl_vector *X_ji, gsl_matrix *J_minus, const gsl_vector *X_ij)
{
    assert (X_ji->size == 3 && X_ji->stride == 1);
    assert (X_ij->size == 3 && X_ij->stride == 1);

    if (J_minus) {
        assert (J_minus->size1 == 3 && J_minus->size2 == 3 && J_minus->tda == 3);
        return xyt_inverse (X_ji->data, J_minus->data, X_ij->data);
    }
    else
        return xyt_inverse (X_ji->data, NULL, X_ij->data);
}

int
xyt_head2tail (double X_ik[3], double J_plus[3*6], const double X_ij[3], const double X_jk[3])
{
  double x_ij = X_ij[0];
  double y_ij = X_ij[1];
  double th_ij = X_ij[2];
  double x_jk = X_jk[0];
  double y_jk = X_jk[1];
  double th_jk = X_jk[2];
  double c_th_ij = cos(th_ij);
  double s_th_ij = sin(th_ij);
  X_ik[0] = x_jk*c_th_ij - y_jk*s_th_ij + x_ij;
  X_ik[1] = x_jk*s_th_ij + y_jk*c_th_ij + y_ij;
  X_ik[2] = th_ij + th_jk;

  if (J_plus != NULL) {
      J_plus[0] = 1;
      J_plus[1] = 0;
      J_plus[2] = -x_jk*s_th_ij - y_jk*c_th_ij;
      J_plus[3] = c_th_ij;
      J_plus[4] = -s_th_ij;
      J_plus[5] = 0;

      J_plus[6] = 0;
      J_plus[7] = 1;
      J_plus[8] = x_jk*c_th_ij - y_jk*s_th_ij;
      J_plus[9] = s_th_ij;
      J_plus[10] = c_th_ij;
      J_plus[11] = 0;
      
      J_plus[12] = 0;
      J_plus[13] = 0;
      J_plus[14] = 1;
      J_plus[15] = 0;
      J_plus[16] = 0;
      J_plus[17] = 1;
  }
  return GSL_SUCCESS;
}

int
xyt_head2tail_gsl (gsl_vector *X_ik, gsl_matrix *J_plus, const gsl_vector *X_ij, const gsl_vector *X_jk)
{
    assert (X_ik->size == 3 && X_ik->stride == 1);
    assert (X_ij->size == 3 && X_ij->stride == 1);
    assert (X_jk->size == 3 && X_jk->stride == 1);

    if (J_plus) {
        assert (J_plus->size1 == 3 && J_plus->size2 == 6 && J_plus->tda == 6);
        return xyt_head2tail (X_ik->data, J_plus->data, X_ij->data, X_jk->data);
    }
    else
        return xyt_head2tail (X_ik->data, NULL, X_ij->data, X_jk->data);
}


int
xyt_tail2tail (double X_jk[3], double J_tail[3*6], const double X_ij[3], const double X_ik[3])
{

  if (J_tail == NULL) {
    double X_ji[3];
    xyt_inverse(X_ji, NULL, X_ij);
    J_tail = NULL;
    xyt_head2tail(X_jk, NULL, X_ji, X_ik);

  }
  else {
    double X_ji[3];
    double J_minus[3*3];
    xyt_inverse(X_ji, J_minus, X_ij);
    
    double J_plus[3*6];
    xyt_head2tail(X_jk, J_plus, X_ji, X_ik);
    
    //J1_plus*J_minus
    J_tail[0] = J_minus[0]*J_plus[0] + J_minus[3]*J_plus[1] + J_minus[6]*J_plus[2];
    J_tail[1] = J_minus[1]*J_plus[0] + J_minus[4]*J_plus[1] + J_minus[7]*J_plus[2];
    J_tail[2] = J_minus[2]*J_plus[0] + J_minus[5]*J_plus[1] + J_minus[8]*J_plus[2];

    J_tail[6] = J_minus[0]*J_plus[6] + J_minus[3]*J_plus[7] + J_minus[6]*J_plus[8];
    J_tail[7] = J_minus[1]*J_plus[6] + J_minus[4]*J_plus[7] + J_minus[7]*J_plus[8];
    J_tail[8] = J_minus[2]*J_plus[6] + J_minus[5]*J_plus[7] + J_minus[8]*J_plus[8];

    J_tail[12] = J_minus[0]*J_plus[12] + J_minus[3]*J_plus[13] + J_minus[6]*J_plus[14];
    J_tail[13] = J_minus[1]*J_plus[12] + J_minus[4]*J_plus[13] + J_minus[7]*J_plus[14];
    J_tail[14] = J_minus[2]*J_plus[12] + J_minus[5]*J_plus[13] + J_minus[8]*J_plus[14];

    //J2_plus 
    J_tail[3] = J_plus[3];
    J_tail[4] = J_plus[4];
    J_tail[5] = J_plus[5];

    J_tail[9] = J_plus[9];
    J_tail[10] = J_plus[10];
    J_tail[11] = J_plus[11];

    J_tail[15] = J_plus[15];
    J_tail[16] = J_plus[16];
    J_tail[17] = J_plus[17];
  }
  return GSL_SUCCESS;
}

int
xyt_tail2tail_gsl (gsl_vector *X_jk, gsl_matrix *J_tail, const gsl_vector *X_ij, const gsl_vector *X_ik)
{
    assert (X_jk->size == 3 && X_jk->stride == 1);
    assert (X_ij->size == 3 && X_ij->stride == 1);
    assert (X_ik->size == 3 && X_ik->stride == 1);

    if (J_tail) {
        assert (J_tail->size1 == 3 && J_tail->size2 == 6 && J_tail->tda == 6);
        return xyt_tail2tail (X_jk->data, J_tail->data, X_ij->data, X_ik->data);
    }
    else
        return xyt_tail2tail (X_jk->data, NULL, X_ij->data, X_ik->data);
}

/*

int main(){
  printf("Testing Inverse...\n\n");

  double X_ji[3];
  double X_ij[3] = {0, 5, 0};
  double J_minus[3*3];

  printf("X_ij: %f, %f, %f\n", X_ij[0], X_ij[1], X_ij[2]);
  xyt_inverse(X_ji, J_minus, X_ij);
  printf("X_ji:\n");
  for(int i=0;i<3;i++){
    printf("%f, ", X_ji[i]);
  }
  printf("\nJ_minus:\n");
  for(int i=0;i<3;i++){
    for(int j=0;j<3;j++)
      printf("%f, ", J_minus[i*3 + j]);
    printf("\n");
  }
  printf("\n\n");
  

  X_ij[0] = 5;
  X_ij[1] = 0;
  X_ij[2] = 0;
  printf("X_ij: %f, %f, %f\n", X_ij[0], X_ij[1], X_ij[2]);
  xyt_inverse(X_ji, J_minus, X_ij);
  printf("X_ji:\n");
  for(int i=0;i<3;i++){
    printf("%f, ", X_ji[i]);
  }
  printf("\nJ_minus:\n");
  for(int i=0;i<3;i++){
    for(int j=0;j<3;j++)
      printf("%f, ", J_minus[i*3 + j]);
    printf("\n");
  }
  printf("\n\n");

  X_ij[0] = 0;
  X_ij[1] = 0;
  X_ij[2] = -3.14159/4;
  printf("X_ij: %f, %f, %f\n", X_ij[0], X_ij[1], X_ij[2]);
  xyt_inverse(X_ji, J_minus, X_ij);
  printf("X_ji:\n");
  for(int i=0;i<3;i++){
    printf("%f, ", X_ji[i]);
  }
  printf("\nJ_minus:\n");
  for(int i=0;i<3;i++){
    for(int j=0;j<3;j++)
      printf("%f, ", J_minus[i*3 + j]);
    printf("\n");
  }
  printf("\n\n");

  X_ij[0] = 2;
  X_ij[1] = 1;
  X_ij[2] = 0.4636;
  printf("X_ij: %f, %f, %f\n", X_ij[0], X_ij[1], X_ij[2]);
  xyt_inverse(X_ji, J_minus, X_ij);
  printf("X_ji:\n");
  for(int i=0;i<3;i++){
    printf("%f, ", X_ji[i]);
  }
  printf("\nJ_minus:\n");
  for(int i=0;i<3;i++){
    for(int j=0;j<3;j++)
      printf("%f, ", J_minus[i*3 + j]);
    printf("\n");
  }
  printf("\n\n");

  printf("\n-------------------------------\nTesting  Head to Tail\n\n");
  X_ij[0] = 0;
  X_ij[1] = 4;
  X_ij[2] = 0;
  double X_jk[3] = {6, 0, 0};
  double X_ik[3];
  double J_plus[3*6];
  xyt_head2tail(X_ik, J_plus, X_ij, X_jk);
  printf("X_ij:\n");
  for(int i=0;i<3;i++){
    printf("%f, ", X_ij[i]);
  }
  printf("\nX_jk:\n");
  for(int i=0;i<3;i++){
    printf("%f, ", X_jk[i]);
  }
  printf("\nX_ik:\n");
  for(int i=0;i<3;i++){
    printf("%f, ", X_ik[i]);
  }
  printf("\nJ_plus:\n");
  for(int i=0;i<3;i++){
    for(int j=0;j<6;j++)
      printf("%f, ", J_plus[i*6 + j]);
    printf("\n");
  }
  printf("\n\n");


  X_ij[0] = 0;
  X_ij[1] = 0;
  X_ij[2] = 3.14159/4;
  X_jk[0] = 0;
  X_jk[1] = 0;
  X_jk[2] = -3.14159/8;
  xyt_head2tail(X_ik, J_plus, X_ij, X_jk);
  printf("X_ij:\n");
  for(int i=0;i<3;i++){
    printf("%f, ", X_ij[i]);
  }
  printf("\nX_jk:\n");
  for(int i=0;i<3;i++){
    printf("%f, ", X_jk[i]);
  }
  printf("\nX_ik:\n");
  for(int i=0;i<3;i++){
    printf("%f, ", X_ik[i]);
  }
  printf("\nJ_plus:\n");
  for(int i=0;i<3;i++){
    for(int j=0;j<6;j++)
      printf("%f, ", J_plus[i*6 + j]);
    printf("\n");
  }
  printf("\n\n");

  X_ij[0] = 0;
  X_ij[1] = 2*sqrt(2);
  X_ij[2] = -3.14159/4;
  X_jk[0] = 2;
  X_jk[1] = 0;
  X_jk[2] = 0;
  xyt_head2tail(X_ik, J_plus, X_ij, X_jk);
  printf("X_ij:\n");
  for(int i=0;i<3;i++){
    printf("%f, ", X_ij[i]);
  }
  printf("\nX_jk:\n");
  for(int i=0;i<3;i++){
    printf("%f, ", X_jk[i]);
  }
  printf("\nX_ik:\n");
  for(int i=0;i<3;i++){
    printf("%f, ", X_ik[i]);
  }
  printf("\nJ_plus:\n");
  for(int i=0;i<3;i++){
    for(int j=0;j<6;j++)
      printf("%f, ", J_plus[i*6 + j]);
    printf("\n");
  }
  printf("\n\n");

  printf("\n-------------------------------\nTesting  Tail to Tail\n\n");
  X_ij[0] = 0;
  X_ij[1] = 4;
  X_ij[2] = 0;
  X_ik[0] = 6;
  X_ik[1] = 4;
  X_ik[2] = 0;
  double J_tail[3*6];
  xyt_tail2tail(X_jk, J_tail, X_ij, X_ik);
  printf("X_ij:\n");
  for(int i=0;i<3;i++){
    printf("%f, ", X_ij[i]);
  }
  printf("\nX_ik:\n");
  for(int i=0;i<3;i++){
    printf("%f, ", X_ik[i]);
  }
  printf("\nX_jk:\n");
  for(int i=0;i<3;i++){
    printf("%f, ", X_jk[i]);
  }
  printf("\nJ_tail\n");
  for(int i=0;i<3;i++){
    for(int j=0;j<6;j++)
      printf("%f, ", J_tail[i*6 + j]);
    printf("\n");
  }
  printf("\n\n");

  X_ij[0] = 0;
  X_ij[1] = 0;
  X_ij[2] = M_PI/4;
  X_ik[0] = 0;
  X_ik[1] = 0;
  X_ik[2] = M_PI/8;
  xyt_tail2tail(X_jk, J_tail, X_ij, X_ik);
  printf("X_ij:\n");
  for(int i=0;i<3;i++){
    printf("%f, ", X_ij[i]);
  }
  printf("\nX_ik:\n");
  for(int i=0;i<3;i++){
    printf("%f, ", X_ik[i]);
  }
  printf("\nX_jk:\n");
  for(int i=0;i<3;i++){
    printf("%f, ", X_jk[i]);
  }
  printf("\nJ_tail\n");
  for(int i=0;i<3;i++){
    for(int j=0;j<6;j++)
      printf("%f, ", J_tail[i*6 + j]);
    printf("\n");
  }
  printf("\n\n");

  X_ij[0] = 0;
  X_ij[1] = 2*sqrt(2);
  X_ij[2] = -M_PI/4;
  X_ik[0] = sqrt(2);
  X_ik[1] = sqrt(2);
  X_ik[2] = -M_PI/4;
  xyt_tail2tail(X_jk, J_tail, X_ij, X_ik);
  printf("X_ij:\n");
  for(int i=0;i<3;i++){
    printf("%f, ", X_ij[i]);
  }
  printf("\nX_ik:\n");
  for(int i=0;i<3;i++){
    printf("%f, ", X_ik[i]);
  }
  printf("\nX_jk:\n");
  for(int i=0;i<3;i++){
    printf("%f, ", X_jk[i]);
  }
  printf("\nJ_tail\n");
  for(int i=0;i<3;i++){
    for(int j=0;j<6;j++)
      printf("%f, ", J_tail[i*6 + j]);
    printf("\n");
  }
  printf("\n\n");

  return 1;
}
*/